<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'orderNumber';

    public function formatData(){

        $productDetailsArray = array();
        $orderTotal = 0;
        foreach ($this->orderProducts as $each_product){

            $product_id = $each_product->productCode;
            $products = Product::where('productCode',$product_id)->firstOrFail();
            $lineTotal =  $each_product->quantityOrdered * $each_product->priceEach  ;
            $tempArray['product'] = $products->productName;
            $tempArray['product_line'] = $products->productLine;
            $tempArray['unit_price'] = $each_product->priceEach;
            $tempArray['qty'] = $each_product->quantityOrdered;
            $tempArray['line_total'] = number_format($lineTotal,2) ;
            $orderTotal = $orderTotal + $lineTotal;
            array_push($productDetailsArray,$tempArray);
        }

        return [
            'order_id' =>$this->orderNumber,
            'order_date' =>$this->orderDate,
            'status' =>$this->status,
            'order_details' =>$productDetailsArray,
            'bill_amount' =>number_format($orderTotal,2),
            'customer' =>[
                'first_name'=>$this->customer->contactFirstName,
                'last_name'=>$this->customer->contactLastName,
                'phone'=>$this->customer->phone,
                'country_code'=>$this->customer->country,
            ],
        ];
    }

    public function customer(){

         return $this->belongsTo(Customer::class,'customerNumber');
    }

    public function orderProducts(){
        return $this->hasMany(OrderDetails::class,'orderNumber');
    }






}
