<?php

namespace App\Repositories;

interface OrderRepositoryInterface
{
    public function getOrderByID($id);
}
