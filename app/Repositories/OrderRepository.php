<?php

namespace App\Repositories;

use App\Order;

class OrderRepository implements OrderRepositoryInterface
{

    public function getOrderByID($id){

        try{
            $orders = Order::where('orderNumber',$id)
                ->with('customer')
                ->with('orderProducts')
                ->firstOrFail()
                ->formatData();
            return response()->json($orders,200);

        } catch (\Exception $error){
            return response()->json(['Status'=>'Error','Message'=>'Sorry ! No results Found for Your Order Id - '.$id.''],400);

        }




    }

}
