<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = 'orderdetails';
    protected $primaryKey = 'orderNumber';

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function product(){
        return $this->hasMany(Product::class,'productCode');
    }

}
