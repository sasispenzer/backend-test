<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'productCode';

    public function order(){
        return $this->belongsToMany(OrderDetails::class);
    }

}
