<?php

namespace App\Http\Controllers;
use App\Repositories\OrderRepositoryInterface;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        $orderDetails = $this->orderRepository->getOrderByID($id);
        return $orderDetails;
    }
}
